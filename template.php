<?php

/**
 * @file
 * template.php
 */

function verybestspirits_theme_bootstrap_search_form_wrapper($variables) {
  $output = '<div class="input-group">';
  $output .= $variables['element']['#children'];
  $output .= '<span class="input-group-btn">';
  $output .= '<button type="submit" class="btn btn-default nav-button">';
  $output .= "<i class='fa fa-search'></i>";
  $output .= '</button>';
  $output .= '</span>';
  $output .= '</div>';
  return $output;
}


function verybestspirits_theme_js_alter(&$js) {
  global $base_url;

  $theme_path = drupal_get_path('theme', 'verybestspirits_theme');

  drupal_add_js(array('gulp_asset' => array(
    'themePath' => $base_url . '/' . $theme_path,
    'imagesPath' => $base_url . '/' . $theme_path . '/gulp-assets/images'
  )
  ), 'setting');

}

var gulp = require('gulp');
var sass = require('gulp-sass');
var livereload = require('gulp-livereload');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

// Sass
gulp.task('sass', function() {
    return gulp.src('sass/gulp-style.sass')
        .pipe(sass({
            outputStyle: 'expanded',
            sourceComments: 'normal',
            includePaths: require('node-neat').includePaths

        }))
        .pipe(gulp.dest('css'));
});

//Parse js files
gulp.task('scripts', function(){
    return gulp.src('js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('js/min'));
});


// Watch Files For Changes
gulp.task('watch', function() {
    livereload.listen();

    gulp.watch(['sass/*.scss', 'sass/*.sass'], ['sass']);
    gulp.watch(['js/*.js'], ['scripts']);

    gulp.watch(['css/**']).on('change', livereload.changed);
    //gulp.watch(['../templates/**']).on('change', livereload.changed);
    gulp.watch(['js/**/*.js']).on('change', livereload.changed);
});

// Default Task
gulp.task('default', ['sass', 'scripts', 'watch']);
